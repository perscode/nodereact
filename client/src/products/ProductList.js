import React from 'react';
import { CardContent } from '../core';


const ProductList = (props) =>  {

    return (
        <div className="content-container">
            <ul>
                {props.products.map((product, i) => 
                    <li key={i}>
                        <div className="card">
                            <CardContent name={product.name} extra={product.inStock} description={product.contain_articles}/>
                            <footer className="card-footer">
                                <button
                                    className={'link card-footer-item fas fa-edit'}
                                    disabled={product.inStock === 0 || props.loading}
                                    aria-label="purchase"
                                    tabIndex={0}
                                    onClick={() => {props.handlePurchase(product)}}
                                    >
                                    <span>
                                        {product.inStock === 0 ? 
                                            'out of stock'
                                            : props.loading
                                            ? 'Please wait...'
                                            : 'Purchase'}
                                    </span>
                                </button>
                            </footer>
                        </div>
                    </li>
                )}
            </ul>
        </div>
    )
}

export default ProductList;