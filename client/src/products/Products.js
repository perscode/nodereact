import React, { useEffect, useState } from 'react';
import ProductList from './ProductList';

const Products = () => {
    const [data, setProducts] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch('/api/products')
            .then(res => res.json())
            .then(setProducts)
            .then(() => setLoading(false))
            .catch(setError);
    }, []);

    const handlePurchase = (props) => {
        setLoading(true);
        fetch(`/api/products/${props.name}`, { method: 'DELETE' })
            .then((res) => res.json())
            .then(setProducts)
            .then(() => setLoading(false))
            .catch(setError);
    }

    if (error) return <pre>{JSON.stringify(error, null, 2)}</pre>
    if (!data) {
        return <div>No data</div>;
    }
    return <ProductList products={data} handlePurchase={handlePurchase} loading={loading}/>
}

export default Products;