import React, { lazy, Suspense } from 'react';
import { Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';
import { HeaderBar, NotFound } from './core';
import './App.css';
import './styles.scss';

const Products = withRouter(
  lazy(() => import(/* webpackChunkName: "products" */ './products/Products'))
);

function App() {
    return (
      <div className="app has-background-grey-dark">
        <HeaderBar />
        <div className="section columns">
          <main className="column">
            <Suspense fallback={<div>Loading...</div>}>
              <BrowserRouter>
                <Switch>
                  <Redirect from="/" exact to="/products" />
                  <Route path="/products" component={Products} />
                  <Route exact path="**" component={NotFound} />
                </Switch>
              </BrowserRouter>
            </Suspense>
          </main>
        </div>
      </div>
    );
}


export default App;
