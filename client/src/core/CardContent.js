import React from 'react';

const CardContent = ({ name, extra, description }) => {
  return (
    <div className="card-content">
      <div className="content">
        <div className="name">{name}</div>
        <div>in stock: {extra}</div>
        <div className="description">
              {
                description.map(article => {
                  return <ul key={article.art_id}>
                    <li>name: {article.name}</li>
                    <li>Parts: {article.amount_of}</li>
                  </ul>
                })
              }
        </div>
      </div>
    </div>
  ) 
};

export default CardContent;