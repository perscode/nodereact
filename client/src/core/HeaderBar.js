import React from 'react';

const HeaderBar = () => (
  <header>
    <nav
      className="navbar has-background-dark is-dark"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <span className="navbar-item">Tech Assessment</span>
      </div>
    </nav>
  </header>
);

export default HeaderBar;