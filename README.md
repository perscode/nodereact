# Node + React

Notes on the project. I decided to treat the json files as "holy", letting strings remain strings (products.json's "amount_of" and inventory.json's "stock" attributes) and interpret the task description as the files were loaded into the software and not a database.
For a webshop like this to scale it certainly wouldn't be optimal to have the time complexity of nested forEach-statements when querying for a product's in stock status. So for this software, in another iteration, I would deffinitely
implement a database. As a personal choice I'd go with mongodb / mongoose. Not a lot of complex relations here, thus a noSQL database certainly would do the trick. 
My compromise for the task was limiting myself on the frontend. Having a background as an angular developer, I decided to write this in React in order to learn something new. 

Notes to my future colleagues that may (or may not) pick up my code: Bundle the server with webpack if hosting on the cloud (so you won't have to move the entire node_modules folder in the process). The environments variable PUBLICWEB is used to point to the client's build-folder - currently hardcoded as client/build. See ./server/index.js
If you decide not to implement a database, then deffinitely make the reading and writing of the files asyncronous and queue requests if a process is already busy accessing the file. I'd also implement infinite scroll / pagination in both the frontend and backend should the number of products available exceed 1000. =)

## Available Scripts

In the project directory, you can run:

### `npm install`

Will run npm install for both the server and the client. Note: the client's node_modules folder is located in the client folder.

### `npm run dev`

Runs the app in the development mode.\ Will launch watch mode for both client and server using [concurrently](https://github.com/open-cli-tools/concurrently#readme). 
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. The api will run on port 8080. Open [http://localhost:8080/api/products](http://localhost:8080/api/products) as an example


### `npm run test`

Will run `npm run test:server` and `npm run test:client`

### `npm run test:server`

Will run tests using jest for the node.js environment.

### `npm run test:client`

Will run tests for the React.js code (there is only 1).

### `npm run build:client`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Launching `npm run start:server` after this allow you to reach the bundled client via the url [http://localhost:8080](http://localhost:8080)

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.



