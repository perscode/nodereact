const express = require('express')
const router = express.Router()

const services = require('../services')
const productService = services.productService

router.get('/products', (req, res) => {
  productService.getProducts(req, res)
})

router.delete('/products/:name', (req, res) => {
  productService.deleteProduct(req, res)
})

module.exports = router
