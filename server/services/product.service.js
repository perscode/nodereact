const fileService = require('./file.service')
const articleService = require('./article.service')
const productsFileName = process.env.PRODUCTS_FILENAME || 'products.json'

/**
 * Reduces the inventory by the amount required by the provided product
 *
 * @param name - A string with the name of the product which the inventory content will be reduced by
 * @returns An array of products with updated inStock count.
 */
function deleteProduct (req, res) {
  const name = req.params.name
  const product = getProductByName(name)
  const articlesToReduce = {}
  product.contain_articles.forEach(article => {
    articlesToReduce[article.art_id] = article.amount_of
  })
  const updatedArticles = articleService.subtractArticlesCount(articlesToReduce)

  articleService.writeFileData(updatedArticles)
  articleService.updateArticlesFromFile()
  getProducts(req, res)
}

/**
 * Returns a product
 *
 * @param name - The name of the requested product
 * @returns A product
 */
function getProductByName (name) {
  if (!name) {
    return new ReferenceError()
  }
  const productList = getProductsFromFile()
  const product = productList.find(a => a.name === name)
  return product
}

/**
 * Returns all product and their stock values
 *
 * @returns A list of all products
 */
function getProducts (req, res) {
  const products = getProductsFromFile()
  setInStockCount(products, (err, response) => {
    if (err) {
      console.log('err', err)
      res.status(500)
    }
    res.json(response)
  })
}

function getProductsFromFile () {
  const response = fileService.getFileData(productsFileName)
  return response.products
}

/**
 * Adds an inStock attribute to the list of products containing the total available coutn based on inventory stock.
 *
 * @param products - Array<Product>
 * @param callback - a callback function
 * @returns Array<Product>
 */
function setInStockCount (products, callback) {
  try {
    articleService.updateArticlesFromFile()
    products.forEach(product => {
      product.inStock = getInStockCount(product)
    })
    callback(null, products)
  } catch (err) {
    callback(err)
  }
}

/**
 * Iterates the a product's list of required articles and returns the max count available
 *
 * @param product - type Product
 * @returns The in stock count of requested product
 */
function getInStockCount (product) {
  let min = -1
  let inStock
  product.contain_articles.forEach(pArticle => {
    const iArticle = articleService.getArticle(pArticle.art_id)
    pArticle.name = iArticle.name
    const count = getAvailableArticleCount(pArticle.amount_of, iArticle.stock)
    if (min === -1) {
      min = count
    } else if (min > count) {
      min = count
    }
    inStock = min
  })
  return inStock
}

/**
 * Returns the number of times a collection of articles can be taken from the inventory
 *
 * @param articleCountRequired - A product's required number of an article
 * @param stock - the number of available articles of the type in stock
 * @returns The count of available products that can be made based on the provided article.
 */
function getAvailableArticleCount (articleCountRequired, stock) {
  const required = parseInt(articleCountRequired, 10)
  const inStock = parseInt(stock, 10)
  const res = inStock / required
  if (isNaN(res)) {
    return 0
  }
  return res < 1 ? 0 : Math.round(res)
}

module.exports = {
  deleteProduct,
  getInStockCount,
  getProducts,
  getProductByName,
  getProductsFromFile
}
