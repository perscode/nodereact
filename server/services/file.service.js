const fs = require('fs')
const path = require('path')
const dataPath = path.resolve('server', 'data')

/**
 * Returns the content of the provided filename. Files must be located in the server/data folder
 *
 * @remarks
 * Syncronous function
 *
 * @param fileName - A string with the name of the requested file
 * @returns The Object, Array, string, number, boolean, or null value corresponding to the given JSON text.
 */
function getFileData (fileName) {
  const response = fs.readFileSync(`${dataPath}/${fileName}`, 'utf8')
  return JSON.parse(response)
}

/**
 * Writes the content of the provided data to a file with the provided filename.
 *
 * @remarks
 * Syncronous function
 *
 * @param data - The data to be written
 * @param fileName - A string with the name of the requested file
 */
function writeFileData (data, fileName) {
  fs.writeFileSync(`${dataPath}/${fileName}`, JSON.stringify(data))
}

module.exports = {
  getFileData,
  writeFileData
}
