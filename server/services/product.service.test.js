const productService = require('./product.service')
const articleService = require('./article.service')

describe('productService', () => {
  beforeEach(() => {
    jest.resetModules()
    process.env.PRODUCTS_FILENAME = 'products.test.json'
  })
  it('should get a list of products', () => {
    // Prepare
    const expectedLength = 2

    // Act
    const products = productService.getProductsFromFile()

    // Assert
    expect(products.length).toEqual(expectedLength)
  })

  it('should return the available count', () => {
    // Prepare
    articleService.updateArticlesFromFile()
    const product1 = {
      name: 'Test sofa 1',
      contain_articles: [
        {
          art_id: '1',
          amount_of: '2'
        },
        {
          art_id: '2',
          amount_of: '1'
        },
        {
          art_id: '3',
          amount_of: '1'
        }
      ]
    }
    const product2 = {
      name: 'Test sofa 1',
      contain_articles: [
        {
          art_id: '1',
          amount_of: '2'
        },
        {
          art_id: '2',
          amount_of: '1'
        }
      ]
    }

    // Act
    const actual1 = productService.getInStockCount(product1)
    const actual2 = productService.getInStockCount(product2)

    // Assert
    expect(actual1).toEqual(2)
    expect(actual2).toEqual(6)
  })
})
