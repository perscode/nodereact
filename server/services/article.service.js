const fileService = require('./file.service')
const fileName = process.env.INVENTORY_FILENAME || 'inventory.json'
let articles

function getInventory () {
  return articles.inventory
}

function updateArticlesFromFile () {
  articles = fileService.getFileData(fileName)
}

/**
 * Returns an array of articles with updated stock values.
 *
 * @remarks
 * articlesToReduce is a js-object where the attr is the id of the article and the value the number to reduce the stock count with.
 *
 * @param articlesToReduce - Input of type object.
 * @returns Array<Article>
 */
function subtractArticlesCount (articlesToReduce) {
  articles.inventory.forEach(article => {
    if (articlesToReduce[article.art_id]) {
      article.stock = (article.stock - articlesToReduce[article.art_id]).toString()
    }
  })
  return articles
}

/**
 * Returns an article with the provided id
 *
 * @param id - of type string
 * @returns Article
 */
function getArticle (id) {
  if (!id) {
    return new ReferenceError()
  }
  const inventory = getInventory()
  const article = inventory.find(a => a.art_id === id)
  return article
}

function writeFileData (data) {
  fileService.writeFileData(data, fileName)
}

module.exports = {
  getArticle,
  getInventory,
  updateArticlesFromFile,
  subtractArticlesCount,
  writeFileData
}
