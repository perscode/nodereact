const fileService = require('./file.service')

describe('readFile', () => {
  it('should read a file from the data folder', () => {
    // Prepare
    const testFile = 'inventory.test.json'
    const leg = {
      art_id: '1',
      name: 'leg',
      stock: '12'
    }
    const screw = {
      art_id: '2',
      name: 'screw',
      stock: '17'
    }

    // Act
    const file = fileService.getFileData(testFile)
    const inventory = file.inventory

    // Assert
    expect(inventory[0].name).toEqual(leg.name)
    expect(inventory[1].stock).toEqual(screw.stock)
  })
})
