const articleService = require('./article.service')

describe('articleService', () => {
  beforeEach(() => {
    jest.resetModules()
    process.env.INVENTORY_FILENAME = 'inventory.test.json'
  })
  it('should get an article by id', () => {
    // Prepare
    const articleId = '2'
    articleService.updateArticlesFromFile()

    // Act
    const article = articleService.getArticle(articleId)

    // Assert
    expect(article.art_id).toEqual(articleId)
  })

  it('should subtract the available articles', () => {
    // Prepare
    articleService.updateArticlesFromFile()
    const articlesToReduces = {
      2: 1, // 17
      3: 2 // 2
    }

    // Act
    const updatedArticles = articleService.subtractArticlesCount(articlesToReduces)

    // Assert
    expect(updatedArticles.inventory[1].stock).toBe('16')
    expect(updatedArticles.inventory[2].stock).toBe('0')
  })
})
