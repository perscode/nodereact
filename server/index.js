const express = require('express')
require('dotenv').config()
const routes = require('./routes')

const publicWeb = process.env.PUBLICWEB || './client/build'
const port = process.env.PORT || 8080

const app = express()
app.use('/api', routes)

app.use(express.static(publicWeb))

app.get('*', (req, res) => {
  res.sendFile('index.html', { root: publicWeb })
})

app.listen(port, () => console.log(`api running on http://localhost:${port}`))

app.use(errorHandler)

function errorHandler (err, req, res, next) {
  res.status(500).send(err)
}
